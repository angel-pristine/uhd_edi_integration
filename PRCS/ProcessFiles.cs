﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OBJT;
using BASE;
using System.Xml;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
using Newtonsoft.Json;
using System.Net;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace PRCS
{
    class ProcessFiles : Utilities
    {
        private string SettlementId = string.Empty;
        private Common _oCommon;
        private List<string> Files = new List<string>();
        private List<ImportMapping> Mappings = new List<ImportMapping>();
        private Connection _oConnection;
        private List<XmlDocument> MyFormattedDocs = new List<XmlDocument>();

        public ProcessFiles(List<string> PFiles, ref Common PCommon, ref Connection PConnection, string PSettlementId) : base(PConnection._oCompany, PCommon)
        {
            _oCommon = PCommon;
            Files = PFiles;
            _oConnection = PConnection;
            SettlementId = PSettlementId;
        }

        public void ReadMapTable()
        {
        restartMethod:

            if (_oConnection._oCompany.Connected == true)
            {
                SAPbobsCOM.Recordset MyRs = _oConnection._oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                try
                {
                    if (_oConnection._oCompany.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                        MyRs.DoQuery(@"SELECT * FROM ""@INN_EDI_IMP_MAP""");
                    else
                        MyRs.DoQuery("SELECT * FROM [@INN_EDI_IMP_MAP]");

                    while (!(MyRs.EoF))
                    {
                        ImportMapping Item = new ImportMapping();
                        Item.Code = Convert.ToInt32(MyRs.Fields.Item(0).Value);
                        Item.Name = MyRs.Fields.Item(1).Value.ToString();
                        Item.IntegrationName = MyRs.Fields.Item(2).Value.ToString();
                        Item.Object = MyRs.Fields.Item(3).Value.ToString();
                        Item.Level = MyRs.Fields.Item(4).Value.ToString();
                        Item.XML_Source = MyRs.Fields.Item(5).Value.ToString();
                        Item.SAP_Target = MyRs.Fields.Item(6).Value.ToString();
                        Item.LookUp = MyRs.Fields.Item(7).Value.ToString();
                        Item.Parameters = MyRs.Fields.Item(8).Value.ToString();
                        Item.Validate = MyRs.Fields.Item(9).Value.ToString();
                        Item.SkipIfEmpty = MyRs.Fields.Item(10).Value.ToString();
                        Item.Enable = MyRs.Fields.Item(11).Value.ToString();
                        Item.MaxOccurs = MyRs.Fields.Item(12).Value.ToString();

                        Mappings.Add(Item);

                        MyRs.MoveNext();
                    }
                }
                catch (Exception Ex)
                {
                    _oCommon.Log("EXCEPTION", Ex.ToString());
                }
                finally
                {
                    if (MyRs != null)
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(MyRs);

                    GC.Collect();

                }

            }
            else
            {
                _oConnection.ConexionSAP();
                goto restartMethod;
            }

        }

        private XmlDocument XlsxToXml(string Pfile)
        {
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                string conn = string.Empty;
                System.Data.DataTable dtexcel = new System.Data.DataTable();
                dtexcel.TableName = "PurchaseOrder";

                conn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Pfile + ";Extended Properties='Excel 12.0;HDR=YES';"; //for above excel 2007  
                using (OleDbConnection con = new OleDbConnection(conn))
                {
                    try
                    {
                        OleDbDataAdapter oleAdpt = new OleDbDataAdapter("SELECT * FROM [ORDR-Header$] WHERE DocNum IS NOT NULL", con); //here we read data from sheet1  
                        oleAdpt.Fill(dtexcel); //fill excel data into dataTable  
                    }
                    catch (Exception Ex) { _oCommon.Log("EXCEPTION", Ex.Message); }

                }

                // Identify which rows does not contains purchase orders
                #region
                List<int> ToRemove = new List<int>();

                foreach (DataRow MyRow in dtexcel.Rows)
                {
                    int numericValue;
                    if (int.TryParse(MyRow["DocNum"].ToString(), out numericValue))
                    {
                        continue;
                    }

                    ToRemove.Add(dtexcel.Rows.IndexOf(MyRow));
                }
                #endregion

                // Sort the indexes from Higher to lower, so it can removes the rows without affecting previous rows indexes. Remove empty, null or no purchase orders rows
                #region
                var nums = ToRemove;

                var enum1 = from num in nums
                            orderby num descending
                            select num;

                foreach (var Value in enum1)
                {
                    dtexcel.Rows.RemoveAt(Value);
                }
                #endregion

                if (dtexcel.Rows.Count > 0)
                {
                    string result;
                    using (StringWriter sw = new StringWriter())
                    {
                        dtexcel.WriteXml(sw);
                        result = sw.ToString();
                        XmlDoc.LoadXml(result);
                    }

                    // Start creation of return XML
                    XmlDocument ResultXml = new XmlDocument();
                    XmlElement Root = ResultXml.CreateElement("Translator");
                    XmlElement POs = ResultXml.CreateElement("POs");
                    XmlElement Info = ResultXml.CreateElement("Info");

                    //Add node for the Information: file name and settlement id
                    #region
                    XmlElement SourceFile = ResultXml.CreateElement("SourceFile");
                    XmlText ValueNode = ResultXml.CreateTextNode(Path.GetFileName(Pfile));
                    SourceFile.AppendChild(ValueNode);
                    Info.AppendChild(SourceFile);

                    XmlElement SettlementNode = ResultXml.CreateElement("SettlementId");
                    ValueNode = ResultXml.CreateTextNode(SettlementId.ToString());
                    SettlementNode.AppendChild(ValueNode);
                    Info.AppendChild(SettlementNode);

                    Root.AppendChild(Info);
                    #endregion

                    foreach (XmlNode XmlTransaction in XmlDoc.SelectNodes("/DocumentElement/PurchaseOrder"))
                    {
                        string parentKey = RetriveXMLValue(XmlTransaction, "./DocNum");

                        System.Data.DataTable dtRows = new System.Data.DataTable();
                        dtRows.TableName = "Row";

                        using (OleDbConnection con = new OleDbConnection(conn))
                        {
                            OleDbDataAdapter oleAdpt;

                            try
                            {
                                oleAdpt = new OleDbDataAdapter("SELECT * FROM [RDR1-Lines$] WHERE ParentKey = " + parentKey, con); //here we read data from sheet1  
                                oleAdpt.Fill(dtRows); //fill excel data into dataTable  
                            }
                            catch (Exception Ex)
                            {
                                if (Ex.Message.Contains("Data type mismatch in criteria expression."))
                                {
                                    try
                                    {
                                        oleAdpt = null;
                                        oleAdpt = new OleDbDataAdapter("SELECT * FROM [RDR1-Lines$] WHERE ParentKey = '" + parentKey + "'", con);
                                        oleAdpt.Fill(dtRows); //fill excel data into dataTable  
                                    }
                                    catch (Exception myEx)
                                    {
                                        _oCommon.Log("EXCEPTION", myEx.Message);
                                    }
                                }
                                else
                                {
                                    _oCommon.Log("EXCEPTION", Ex.Message);
                                }

                            }
                        }

                        XmlDocument Rows = new XmlDocument();
                        string RowString;
                        using (StringWriter sw = new StringWriter())
                        {
                            dtRows.WriteXml(sw);
                            RowString = sw.ToString();
                            Rows.LoadXml(RowString);
                        }

                        foreach (XmlNode MyRow in Rows.SelectNodes("/DocumentElement"))
                        {
                            XmlNode importNode = XmlTransaction.OwnerDocument.ImportNode(MyRow, true);
                            XmlTransaction.AppendChild(importNode);
                        }

                        XmlNode RetTransaction = ResultXml.ImportNode(XmlTransaction, true);
                        POs.AppendChild(RetTransaction);
                    }

                    Root.AppendChild(POs);

                    ResultXml.AppendChild(Root);

                    return ResultXml;
                }
                else
                {
                    SendMailExeptions("Invalid format on this file, please check the values on the excel. This file will be omitted", Pfile);
                    return null;
                }
            }
            catch (Exception Ex)
            {
                _oCommon.Log("EXCEPTION", Ex.ToString());
                SendMailExeptions(Ex.Message, Pfile);
                return null;
            }

        }

        private XmlDocument GroupOrders(XmlDocument PDocument, string Pfile)
        {
            try
            {
                List<GroupRow> MyRows = new List<GroupRow>();

                // Retrieve all row Line nodes into objects
                foreach (XmlNode XmlTransaction in PDocument.SelectNodes("/Translator/POs/PurchaseOrder/DocumentElement/Row"))
                {
                    GroupRow MyRow = new GroupRow();

                    MyRow.ParentKey = RetriveXMLValue(XmlTransaction, "./ParentKey");
                    MyRow.LineNum = RetriveXMLValue(XmlTransaction, "./LineNum");
                    MyRow.ItemCode = RetriveXMLValue(XmlTransaction, "./SupplierCatNum");
                    MyRow.ItemDescription = RetriveXMLValue(XmlTransaction, "./ItemDescription");
                    MyRow.Quantity = RetriveXMLValue(XmlTransaction, "./Quantity");
                    MyRow.ShipDate = RetriveXMLValue(XmlTransaction, "./ShipDate");
                    MyRow.Price = RetriveXMLValue(XmlTransaction, "./Price");
                    MyRow.Currency = RetriveXMLValue(XmlTransaction, "./Currency");
                    MyRow.WarehouseCode = RetriveXMLValue(XmlTransaction, "./WarehouseCode");
                    MyRow.TaxCode = RetriveXMLValue(XmlTransaction, "./TaxCode");
                    MyRow.LineType = RetriveXMLValue(XmlTransaction, "./LineType");
                    MyRow.U_x0020_Store_x0020_Id = RetriveXMLValue(XmlTransaction, "./U_Store_Id");
                    MyRow.U_x0020_Customer_x0020_PO = RetriveXMLValue(XmlTransaction, "./U_Customer_PO");

                    MyRows.Add(MyRow);
                }

                //Get Header Node removing the Document Element which contains row line nodes
                XmlNode HeaderNodeBase = PDocument.SelectSingleNode("/Translator/POs/PurchaseOrder");

                foreach (XmlNode XmlTransaction in HeaderNodeBase.SelectNodes("/Translator/POs/PurchaseOrder/DocumentElement"))
                {
                    if (!string.IsNullOrEmpty(XmlTransaction.InnerText))
                    {
                        HeaderNodeBase.RemoveChild(XmlTransaction);
                    }

                }

                // Group all rows by Customer PO Field
                var results = MyRows.GroupBy(p => p.U_x0020_Customer_x0020_PO);

                // Start creation of return XML
                XmlDocument ResultXml = new XmlDocument();
                XmlElement Root = ResultXml.CreateElement("Translator");
                XmlElement POs = ResultXml.CreateElement("POs");
                XmlElement Info = ResultXml.CreateElement("Info");

                //Add node for the Information: file name and settlement id
                #region
                XmlElement SourceFile = ResultXml.CreateElement("SourceFile");
                XmlText ValueNode = ResultXml.CreateTextNode(Path.GetFileName(Pfile));
                SourceFile.AppendChild(ValueNode);
                Info.AppendChild(SourceFile);

                XmlElement SettlementNode = ResultXml.CreateElement("SettlementId");
                ValueNode = ResultXml.CreateTextNode(SettlementId.ToString());
                SettlementNode.AppendChild(ValueNode);
                Info.AppendChild(SettlementNode);

                Root.AppendChild(Info);
                #endregion

                foreach (var item in results)
                {
                    XmlDocument RowDocument = new XmlDocument();
                    RowDocument.LoadXml("<Order>" + HeaderNodeBase.InnerXml + "</Order>");

                    XmlNode HeaderNode = RowDocument.SelectSingleNode("/Order");
                    XmlElement Rows = RowDocument.CreateElement("Rows");

                    string CustPo = string.Empty;

                    foreach (var child in item)
                    {
                        XmlDocument ChildXml = new XmlDocument();
                        CustPo = child.U_x0020_Customer_x0020_PO.ToString();

                        using (StringWriter sw = new StringWriter())
                        {
                            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(child.GetType());
                            string XMLSerialized;
                            x.Serialize(sw, child);
                            XMLSerialized = sw.ToString();
                            ChildXml.LoadXml(XMLSerialized);

                            foreach (XmlNode MyRow in ChildXml.SelectNodes("/GroupRow"))
                            {
                                XmlNode importChild = HeaderNode.OwnerDocument.ImportNode(MyRow, true);
                                Rows.AppendChild(importChild);
                            }

                        }

                        HeaderNode.AppendChild(Rows);

                    }

                    XmlElement U_x0020_Customer_x0020_PO = RowDocument.CreateElement("U_x0020_Customer_x0020_PO");
                    XmlText U_x0020_Customer_x0020_PO_Text = RowDocument.CreateTextNode(CustPo);
                    U_x0020_Customer_x0020_PO.AppendChild(U_x0020_Customer_x0020_PO_Text);
                    HeaderNode.AppendChild(U_x0020_Customer_x0020_PO);

                    XmlNode importHeader = POs.OwnerDocument.ImportNode(HeaderNode, true);
                    POs.AppendChild(importHeader);
                }

                Root.AppendChild(POs);

                ResultXml.AppendChild(Root);

                string filename = Path.GetFileNameWithoutExtension(Pfile);
                string savePath = Innormax_14.Properties.Settings.Default.XMLFolder + "\\" + filename + "#" + SettlementId + ".xml";
                ResultXml.Save(savePath);

                return ResultXml;
            }
            catch (Exception Ex)
            {
                _oCommon.Log("EXCEPTION", Ex.ToString());
                SendMailExeptions(Ex.Message, Pfile);
                return null;
            }

        }

        public void FormatDocumentXLSX()
        {
            List<LogRecord> SuccesRecords = new List<LogRecord>();
            List<LogRecord> ErrorRecords = new List<LogRecord>();
            List<LogRecord> eXceptionRecords = new List<LogRecord>();
            List<string> ExistingRecords = new List<string>();
            List<string> ProcessErrors = new List<string>();
            int GeneralCounter = 0;

            if (Mappings.Count > 0)
            {
                // For each file found on folder
                foreach (string file in Files)
                {
                    XmlDocument UHDDoc = new XmlDocument();
                    UHDDoc = XlsxToXml(file);

                    if (UHDDoc == null)
                        continue;

                    UHDDoc = GroupOrders(UHDDoc, file);

                    if (UHDDoc != null)
                    {
                        // For each transaction on every file
                        foreach (XmlNode XmlTransaction in UHDDoc.SelectNodes("/Translator/POs/Order"))
                        {
                            GeneralCounter++;
                            string PurchaseOrderNumber = RetriveXMLValue(XmlTransaction, "./U_x0020_Customer_x0020_PO");
                            string ExistingDoc = string.Empty;

                            if (PurchaseOrderNumber != null)
                            {
                                if (_oConnection._oCompany.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                                {
                                    ExistingDoc = GetFieldValue(string.Format(@"SELECT ""DocNum"" FROM ""{0}"" WHERE ""{1}"" = '{2}' AND ""CANCELED"" = 'N'", Innormax_14.Properties.Settings.Default.SAPObjTable, Innormax_14.Properties.Settings.Default.ValidationField, PurchaseOrderNumber));
                                }
                                else
                                {
                                    ExistingDoc = GetFieldValue(string.Format(@"SELECT DocNum FROM {0} WHERE {1} = '{2}' AND CANCELED = 'N'", Innormax_14.Properties.Settings.Default.SAPObjTable, Innormax_14.Properties.Settings.Default.ValidationField, PurchaseOrderNumber));
                                }

                                if (!string.IsNullOrEmpty(ExistingDoc))
                                {
                                    XmlNodeList xnList = XmlTransaction.SelectNodes("./Rows/GroupRow");

                                    ValidateOrder RetOrder = ValidateFullOrder(xnList, PurchaseOrderNumber);

                                    if (RetOrder != null)
                                    {
                                        _oCommon.Log("Start", "Transaction: " + PurchaseOrderNumber + " - File: " + Path.GetFileName(file));
                                        _oCommon.Log("INFO", "Purchase Order Number " + PurchaseOrderNumber + " already on SAP with DocNum: " + RetOrder.DocNum + ", Posted: " + RetOrder.CreateDate + " - " + RetOrder.CreateTS);
                                        _oCommon.Log("End", "Transaction finished");
                                        ExistingRecords.Add("Purchase Order Number " + PurchaseOrderNumber + " already on SAP with DocNum: " + RetOrder.DocNum + ", Posted: " + RetOrder.CreateDate + " - " + RetOrder.CreateTS);
                                        continue;
                                    }
                                }
                            }
                            else
                            {
                                _oCommon.Log("Start", "Transaction: NA " + " - File: " + Path.GetFileName(file));
                                _oCommon.Log("INFO", "Null identifier retrieved, field configured: " + Innormax_14.Properties.Settings.Default.ValidationField);
                                _oCommon.Log("End", "Transaction finished");
                                ProcessErrors.Add("Null identifier retrieved, field configured: " + Innormax_14.Properties.Settings.Default.ValidationField);
                                continue;
                            }

                            LogRecord TransRecord = new LogRecord();

                            try
                            {
                                _oCommon.Log("Start", "Transaction: " + PurchaseOrderNumber + " - File: " + Path.GetFileName(file));

                                XmlDocument SAPDoc = new XmlDocument();

                                CreateSAPHeader(ref SAPDoc);

                                // Create Header of document
                                CreateDocumentHeader(ref SAPDoc, XmlTransaction, Mappings);

                                // Create Address Extension of the document
                                CreateDocumentAddressExtension(ref SAPDoc, XmlTransaction, Mappings);

                                // Creation of Document_Lines
                                XmlElement Documents = SAPDoc.CreateElement("Document_Lines");
                                XmlNode BO = SAPDoc.SelectSingleNode("/BOM/BO");
                                BO.AppendChild(Documents);

                                XmlNodeList xnList = XmlTransaction.SelectNodes("./Rows/GroupRow");

                                foreach (XmlNode XmlLine in xnList)
                                {
                                    CreateDocumentLine(ref SAPDoc, XmlTransaction, XmlLine, Mappings);
                                }

                                // Print idented XML process
                                #region Print idented XML process
                                MemoryStream mStream = new MemoryStream();
                                XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);

                                writer.Formatting = System.Xml.Formatting.Indented;

                                SAPDoc.WriteContentTo(writer);
                                writer.Flush();
                                mStream.Flush();

                                mStream.Position = 0;

                                StreamReader sReader = new StreamReader(mStream);

                                // Extract the text from the StreamReader.
                                string formattedXml = sReader.ReadToEnd();
                                #endregion

                                // Print idented XML Document
                                _oCommon.Log("INFO", formattedXml);

                                // Log Record Properties assignament
                                TransRecord.PurchaseOrderNumber = PurchaseOrderNumber;
                                TransRecord.SettlementId = SettlementId;
                                TransRecord.CardCode = RetriveXMLValue(BO, "./Documents/row/CardCode");
                                TransRecord.FileName = file;
                                TransRecord.DocumentDate = RetriveXMLValue(BO, "./Documents/row/DocDate");
                                TransRecord.Owner = Innormax_14.Properties.Settings.Default.Owner;

                                // Post of the XML Document
                                PostDocument(ref SAPDoc, ref TransRecord);

                            }
                            catch (Exception Ex)
                            {
                                _oCommon.Log("EXCEPTION", Ex.ToString());
                                TransRecord.Status = "X";
                                TransRecord.Description = Ex.ToString();
                            }
                            finally
                            {
                                LogTable(TransRecord);

                                if (TransRecord.Status == "S")
                                    SuccesRecords.Add(TransRecord);
                                else if (TransRecord.Status == "X")
                                    eXceptionRecords.Add(TransRecord);
                                else
                                    ErrorRecords.Add(TransRecord);

                                _oCommon.Log("End", "Transaction finished");
                                TransRecord = null;
                            }
                        }
                    }
                    else
                    {
                        _oCommon.Log("INFO", "No XML File returned from convertion method");
                    }

                    UHDDoc = null;
                }

            }
            else
            {
                _oCommon.Log("INFO", "No Mappings retrieved from Mapping Table");
            }

            if (GeneralCounter > 0)
            {
                SendMail(SuccesRecords, ErrorRecords, eXceptionRecords, ExistingRecords, ProcessErrors, GeneralCounter);
            }


            foreach (string file in Files)
            {
                MoveFile(file);
            }


        }

        private bool SendMailExeptions(string Exception, string PFile)
        {
            try
            {
                List<string> MailReceibers = Innormax_14.Properties.Settings.Default.MailDestinations.Split(';').ToList();

                string FileNames = string.Empty;
                foreach (string filename in Files)
                {
                    FileNames = FileNames + filename + " - ";
                }

                String Servidor = "smtp.office365.com";
                int Puerto = 587;

                String GmailUser = Innormax_14.Properties.Settings.Default.MailSender;
                String GmailPass = "Innormax2022";

                MimeMessage mensaje = new MimeMessage();
                mensaje.From.Add(new MailboxAddress("Innormax Notifications", GmailUser));

                // Set recipient email address
                foreach (string address in MailReceibers)
                {
                    mensaje.To.Add(new MailboxAddress("Destino", address));
                }

                BodyBuilder CuerpoMensaje = new BodyBuilder();

                // Set email body
                CuerpoMensaje.HtmlBody = @"Automatic execution notification, please read below: <br />
                        Owner: " + Innormax_14.Properties.Settings.Default.Owner + @" <br />
                        Integration: Purchase Orders TCM integration - UHD XLSX files provider <br />
                        Execution : " + DateTime.Now.ToString() + @" <br /> 
                        <br /> 
                        <br />
                        Description: <br />
                        <br />
                        FileName(s): " + Path.GetFileName(PFile) + @" <br />";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + " <br /> ";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + Exception.ToString() + " <br /> ";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + " <br /> ";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + @"For more information, please refer to log files and log table. <br />
                                    If you need additional assistance, please contact your system administrator. <br /> 
                                    This is an automatic notification, do not response this mail.";

                mensaje.Body = CuerpoMensaje.ToMessageBody();

                SmtpClient ClienteSmtp = new SmtpClient();
                ClienteSmtp.CheckCertificateRevocation = false;
                ClienteSmtp.Connect(Servidor, Puerto, MailKit.Security.SecureSocketOptions.StartTls);
                ClienteSmtp.Authenticate(GmailUser, GmailPass);

                _oCommon.Log("INFO", "start to send email over TLS...");

                ClienteSmtp.Send(mensaje);
                ClienteSmtp.Disconnect(true);

                _oCommon.Log("INFO", "email was sent successfully!");

                return true;
            }
            catch (Exception MyEx)
            {
                _oCommon.Log("Exception", MyEx.Message);
                return false;
            }
        }

        private bool SendMail(List<LogRecord> PSuccesRecords, List<LogRecord> PErrorRecords, List<LogRecord> PeXceptionRecords, List<string> PExistingRecords, List<string> PProcessErrors, int PGeneralCounter)
        {
            try
            {
                List<string> MailReceibers = Innormax_14.Properties.Settings.Default.MailDestinations.Split(';').ToList();

                string FileNames = string.Empty;
                foreach (string filename in Files)
                {
                    FileNames = FileNames + filename + " - ";
                }

                String Servidor = "smtp.office365.com";
                int Puerto = 587;

                String GmailUser = Innormax_14.Properties.Settings.Default.MailSender;
                String GmailPass = "Innormax2022";

                MimeMessage mensaje = new MimeMessage();
                mensaje.From.Add(new MailboxAddress("Innormax Notifications", GmailUser));

                // Set recipient email address
                foreach (string address in MailReceibers)
                {
                    mensaje.To.Add(new MailboxAddress("Destino", address));
                }

                mensaje.Subject = "Purchase Orders Integration Execution - " + DateTime.Now.ToShortDateString();

                BodyBuilder CuerpoMensaje = new BodyBuilder();

                // Set email body
                CuerpoMensaje.HtmlBody = @"Automatic execution notification, please read below: <br />
                        Owner: " + Innormax_14.Properties.Settings.Default.Owner + @" <br />
                        Integration: Purchase Orders TCM integration - UHD XLSX files provider <br />
                        Execution : " + DateTime.Now.ToString() + @" <br /> 
                        <br /> 
                        <br />
                        Description: <br />
                        <br />
                        FileName(s): " + FileNames + @" <br />
                        Total Transaction retrieved: " + PGeneralCounter.ToString() + @" <br />
                        <br /> 
                        Succes Records: " + PSuccesRecords.Count.ToString() + @" <br /> ";

                foreach (LogRecord Succes in PSuccesRecords)
                {
                    CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + "Purchase Order: " + Succes.PurchaseOrderNumber + " - DocNum: " + Succes.DocNum + " - DocEntry: " + Succes.DocEntry + " <br /> ";
                }

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + " <br /> ";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + "Existing Records: " + PExistingRecords.Count.ToString() + " <br /> ";

                foreach (string Existing in PExistingRecords)
                {
                    CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + Existing + " <br /> ";
                }

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + " <br /> ";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + "Process Errors: " + PProcessErrors.Count.ToString() + " <br /> ";

                foreach (string PError in PProcessErrors)
                {
                    CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + PError + " <br /> ";
                }

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + " <br /> ";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + "Error Records: " + PErrorRecords.Count.ToString() + " <br /> ";

                foreach (LogRecord Error in PErrorRecords)
                {
                    CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + "Purchase Order: " + Error.PurchaseOrderNumber + " - Error Description: " + Error.Description + " <br /> ";
                }

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + " <br /> ";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + "Exception Records: " + PeXceptionRecords.Count.ToString() + " <br /> ";

                foreach (LogRecord Exception in PeXceptionRecords)
                {
                    CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + "Purchase Order: " + Exception.PurchaseOrderNumber + " - Exception Description: " + Exception.Description + " <br /> ";
                }

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + " <br /> ";

                CuerpoMensaje.HtmlBody = CuerpoMensaje.HtmlBody + @"For more information, please refer to log files and log table. <br />
                                    If you need additional assistance, please contact your Innormax system administrator. <br /> 
                                    This is an automatic notification, do not response this mail.";

                mensaje.Body = CuerpoMensaje.ToMessageBody();

                SmtpClient ClienteSmtp = new SmtpClient();
                ClienteSmtp.CheckCertificateRevocation = false;
                ClienteSmtp.Connect(Servidor, Puerto, MailKit.Security.SecureSocketOptions.StartTls);
                ClienteSmtp.Authenticate(GmailUser, GmailPass);

                _oCommon.Log("INFO", "start to send email over TLS...");

                ClienteSmtp.Send(mensaje);
                ClienteSmtp.Disconnect(true);

                _oCommon.Log("INFO", "email was sent successfully!");

                return true;
            }
            catch (Exception MyEx)
            {
                _oCommon.Log("Exception", MyEx.Message);
                return false;
            }
        }

        public void FormatDocumentXML()
        {
            if (Mappings.Count > 0)
            {
                // For each file found on folder
                foreach (string file in Files)
                {
                    XmlDocument UHDDoc = new XmlDocument();
                    UHDDoc.Load(file);

                    // For each transaction on every file
                    foreach (XmlNode XmlTransaction in UHDDoc.SelectNodes("/BODS/BOD/PurchaseOrder"))
                    {
                        string PurchaseOrderNumber = RetriveXMLValue(XmlTransaction, "./PurchaseOrderHeader/PurchaseOrderNumber");

                        string ExistingDoc = GetFieldValue(string.Format(@"SELECT DocNum FROM {0} WHERE U_Cust_PO_No = '{1}'", Innormax_14.Properties.Settings.Default.SAPObjTable, PurchaseOrderNumber));

                        if (ExistingDoc != null)
                        {
                            _oCommon.Log("Start", "Transaction: " + PurchaseOrderNumber + " - File: " + Path.GetFileName(file));
                            _oCommon.Log("INFO", "Purchase Order Number already posted on SAP with DocNum: " + ExistingDoc);
                            _oCommon.Log("End", "Transaction finished");
                            continue;
                        }

                        LogRecord TransRecord = new LogRecord();

                        try
                        {
                            _oCommon.Log("Start", "Transaction: " + PurchaseOrderNumber + " - File: " + Path.GetFileName(file));

                            XmlDocument SAPDoc = new XmlDocument();

                            CreateSAPHeader(ref SAPDoc);

                            // Create Header of document
                            CreateDocumentHeader(ref SAPDoc, XmlTransaction, Mappings);

                            // Create Address Extension of the document
                            CreateDocumentAddressExtension(ref SAPDoc, XmlTransaction, Mappings);

                            // Creation of Document_Lines
                            XmlElement Documents = SAPDoc.CreateElement("Document_Lines");
                            XmlNode BO = SAPDoc.SelectSingleNode("/BOM/BO");
                            BO.AppendChild(Documents);

                            XmlNodeList xnList = XmlTransaction.SelectNodes("./PurchaseOrderLine");

                            foreach (XmlNode XmlLine in xnList)
                            {
                                CreateDocumentLine(ref SAPDoc, XmlTransaction, XmlLine, Mappings);
                            }

                            // Print idented XML process
                            #region Print idented XML process
                            MemoryStream mStream = new MemoryStream();
                            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);

                            writer.Formatting = System.Xml.Formatting.Indented;

                            SAPDoc.WriteContentTo(writer);
                            writer.Flush();
                            mStream.Flush();

                            mStream.Position = 0;

                            StreamReader sReader = new StreamReader(mStream);

                            // Extract the text from the StreamReader.
                            string formattedXml = sReader.ReadToEnd();
                            #endregion

                            // Print idented XML Document
                            _oCommon.Log("INFO", formattedXml);

                            // Log Record Properties assignament
                            TransRecord.PurchaseOrderNumber = PurchaseOrderNumber;
                            TransRecord.SettlementId = SettlementId;
                            TransRecord.CardCode = RetriveXMLValue(BO, "./Documents/row/CardCode");
                            TransRecord.FileName = file;
                            TransRecord.DocumentDate = RetriveXMLValue(BO, "./Documents/row/DocDate");
                            TransRecord.Owner = Innormax_14.Properties.Settings.Default.Owner;

                            // Post of the XML Document
                            PostDocument(ref SAPDoc, ref TransRecord);

                        }
                        catch (Exception Ex)
                        {
                            _oCommon.Log("EXCEPTION", Ex.ToString());
                            TransRecord.Status = "X";
                            TransRecord.Description = Ex.ToString();
                        }
                        finally
                        {
                            LogTable(TransRecord);
                            _oCommon.Log("End", "Transaction finished");
                            TransRecord = null;
                        }
                    }

                    UHDDoc = null;
                }

            }
            else
            {
                _oCommon.Log("INFO", "No Mappings retrieved from Mapping Table");
            }
        }

        private void CreateSAPHeader(ref XmlDocument SAPDoc)
        {
            // Create BOM Node
            XmlElement BOM = SAPDoc.CreateElement("BOM");
            // Create BO Node
            XmlElement BO = SAPDoc.CreateElement("BO");
            // Create AdmInfo Node
            XmlElement AdmInfo = SAPDoc.CreateElement("AdmInfo");
            // Create Object Node
            XmlElement Object = SAPDoc.CreateElement("Object");
            XmlText Value = SAPDoc.CreateTextNode(Innormax_14.Properties.Settings.Default.SAPObjType.ToString());
            Object.AppendChild(Value);
            AdmInfo.AppendChild(Object);
            // Create Object Node
            XmlElement Version = SAPDoc.CreateElement("Version");
            Value = SAPDoc.CreateTextNode("2");
            Version.AppendChild(Value);
            AdmInfo.AppendChild(Version);
            // Append Node
            BO.AppendChild(AdmInfo);
            BOM.AppendChild(BO);
            SAPDoc.AppendChild(BOM);
        }

        private void PostDocument(ref XmlDocument SAPDoc, ref LogRecord MyRecord)
        {
            _oCommon.Log("INFO", "Adding Document");
            SAPbobsCOM.Documents oDoc = default(SAPbobsCOM.Documents);

            try
            {
                _oConnection._oCompany.XMLAsString = true;
                _oConnection._oCompany.XmlExportType = SAPbobsCOM.BoXmlExportTypes.xet_ExportImportMode;

                oDoc = (SAPbobsCOM.Documents)_oConnection._oCompany.GetBusinessObjectFromXML(SAPDoc.OuterXml.ToString(), 0);

                if (oDoc.Add() != 0)
                {
                    _oCommon.Log("ERROR", _oConnection._oCompany.GetLastErrorCode() + " - " + _oConnection._oCompany.GetLastErrorDescription());
                    MyRecord.Status = "E";
                    MyRecord.Description = _oConnection._oCompany.GetLastErrorCode() + " - " + _oConnection._oCompany.GetLastErrorDescription();

                }
                else
                {
                    string DocNum = string.Empty;

                    if (_oConnection._oCompany.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                    {
                        DocNum = GetFieldValue(string.Format(@"SELECT ""DocNum"" FROM ""{0}"" WHERE ""DocEntry"" = {1}", Innormax_14.Properties.Settings.Default.SAPObjTable, _oConnection._oCompany.GetNewObjectKey()));
                    }
                    else
                    {
                        DocNum = GetFieldValue(string.Format(@"SELECT DocNum FROM {0} WHERE DocEntry = {1}", Innormax_14.Properties.Settings.Default.SAPObjTable, _oConnection._oCompany.GetNewObjectKey()));
                    }


                    MyRecord.DocNum = DocNum;
                    MyRecord.DocEntry = _oConnection._oCompany.GetNewObjectKey();
                    MyRecord.Status = "S";

                    _oCommon.Log("Success", "Document posted with DocEntry " + _oConnection._oCompany.GetNewObjectKey() + " - Document Number: " + DocNum);

                }

            }
            catch (Exception Ex)
            {
                _oCommon.Log("Exception", Ex.Message);
                MyRecord.Status = "X";
                MyRecord.Description = Ex.Message;
            }
            finally
            {
                if (oDoc != null)
                    Marshal.ReleaseComObject(oDoc);
            }
        }

        private void CreateDocumentHeader(ref XmlDocument PSAPDoc, XmlNode PXmlTransaction, List<ImportMapping> PMappings)
        {
            string NodeString;
            string value;
            XmlElement Object;
            XmlText Value;

            XmlElement Documents = PSAPDoc.CreateElement("Documents");
            XmlElement Row = PSAPDoc.CreateElement("row");
            Documents.AppendChild(Row);

            // Select Node were we are going to nest the header values
            XmlNode BO = PSAPDoc.SelectSingleNode("/BOM/BO");
            BO.AppendChild(Documents);

            foreach (ImportMapping Mapping in Mappings)
            {
                if (!Mapping.Level.Equals("H"))
                {
                    continue;
                }

                NodeString = string.Empty;

                value = GetSingleValueObject(PXmlTransaction.GetType(), PXmlTransaction, Mapping.XML_Source, Mapping.SAP_Target, Mapping.LookUp, Mapping.Parameters, Mapping.Validate, Innormax_14.Properties.Settings.Default.SAPObjType.ToString());

                if (string.IsNullOrEmpty(value) && Mapping.SkipIfEmpty.Equals("y", StringComparison.InvariantCultureIgnoreCase))
                    continue;
                else if (Mapping.Enable.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                    continue;
                else
                {
                    Object = PSAPDoc.CreateElement(Mapping.SAP_Target);
                    Value = PSAPDoc.CreateTextNode(value.Replace("&", "&amp;"));
                    Object.AppendChild(Value);
                    Row.AppendChild(Object);
                }

                Object = null;
                Value = null;
            }

            // Adding Settlement ID to header
            Object = PSAPDoc.CreateElement("U_Settlement_ID");
            Value = PSAPDoc.CreateTextNode(SettlementId);
            Object.AppendChild(Value);
            Row.AppendChild(Object);

            // Adding Integration Origin
            Object = PSAPDoc.CreateElement("U_Integration");
            Value = PSAPDoc.CreateTextNode("Y");
            Object.AppendChild(Value);
            Row.AppendChild(Object);

        }

        private void CreateDocumentAddressExtension(ref XmlDocument PSAPDoc, XmlNode PXmlTransaction, List<ImportMapping> PMappings)
        {
            string NodeString;
            string value;
            XmlElement Object;
            XmlText Value;

            XmlElement Documents = PSAPDoc.CreateElement("AddressExtension");
            XmlElement Row = PSAPDoc.CreateElement("row");
            Documents.AppendChild(Row);

            // Select Node were we are going to nest the header values
            XmlNode BO = PSAPDoc.SelectSingleNode("/BOM/BO");
            BO.AppendChild(Documents);

            foreach (ImportMapping Mapping in Mappings)
            {
                if (!Mapping.Level.Equals("A"))
                {
                    continue;
                }

                NodeString = string.Empty;

                value = GetSingleValueObject(PXmlTransaction.GetType(), PXmlTransaction, Mapping.XML_Source, Mapping.SAP_Target, Mapping.LookUp, Mapping.Parameters, Mapping.Validate, Innormax_14.Properties.Settings.Default.SAPObjType.ToString());

                if (string.IsNullOrEmpty(value) && Mapping.SkipIfEmpty.Equals("y", StringComparison.InvariantCultureIgnoreCase))
                    continue;
                else if (Mapping.Enable.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                    continue;
                else
                {
                    Object = PSAPDoc.CreateElement(Mapping.SAP_Target);
                    Value = PSAPDoc.CreateTextNode(value.Replace("&", "&amp;"));
                    Object.AppendChild(Value);
                    Row.AppendChild(Object);
                }

                Object = null;
                Value = null;
            }
        }

        private void CreateDocumentLine(ref XmlDocument PSAPDoc, XmlNode PXmlTransaction, XmlNode PXmlLine, List<ImportMapping> PMappings)
        {
            string NodeString;
            string value;
            XmlElement Object;
            XmlText Value;

            XmlElement Row = PSAPDoc.CreateElement("row");

            // Select Node were we are going to nest the Line row
            XmlNode Document_Lines = PSAPDoc.SelectSingleNode("/BOM/BO/Document_Lines");
            Document_Lines.AppendChild(Row);

            foreach (ImportMapping Mapping in Mappings)
            {
                if (!Mapping.Level.Equals("L"))
                {
                    continue;
                }

                NodeString = string.Empty;

                value = GetLineValueObject(PXmlTransaction, PXmlLine, Mapping.XML_Source, Mapping.SAP_Target, Mapping.LookUp, Mapping.Parameters, Mapping.Validate, Innormax_14.Properties.Settings.Default.SAPObjType.ToString());

                if (string.IsNullOrEmpty(value) && Mapping.SkipIfEmpty.Equals("y", StringComparison.InvariantCultureIgnoreCase))
                    continue;
                else if (Mapping.Enable.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                    continue;
                else
                {
                    Object = PSAPDoc.CreateElement(Mapping.SAP_Target);
                    Value = PSAPDoc.CreateTextNode(value.Replace("&", "&amp;"));
                    Object.AppendChild(Value);
                    Row.AppendChild(Object);
                }

                Object = null;
                Value = null;
            }
        }

        private bool MoveFile(string PPath)
        {
            try
            {
                // Ensure that the target does not exist.
                if (File.Exists(Innormax_14.Properties.Settings.Default.SuccessFolder + "/" + Path.GetFileName(PPath)))
                    File.Delete(Innormax_14.Properties.Settings.Default.SuccessFolder + "/" + Path.GetFileName(PPath));

                // Move the file.
                File.Move(PPath, Innormax_14.Properties.Settings.Default.SuccessFolder + "/" + Path.GetFileName(PPath));

                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

            return false;
        }

        private ValidateOrder ValidateFullOrder(XmlNodeList ItemNodes, string U_Cust_PO_No)
        {
            ValidateOrder SapOrder = null;
            ValidateOrder ExcelOrder = new ValidateOrder();

            try
            {
                SAPbobsCOM.Recordset MyRs = _oConnection._oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                string query = string.Empty;
                List<string> FoundDocEntries = new List<string>();

                if (_oConnection._oCompany.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                {
                    query = @"SELECT T0.""DocEntry"" FROM ORDR T0 WHERE T0.""CANCELED"" = 'N' AND T0.""U_Cust_PO_No"" = '" + U_Cust_PO_No + "'";
                }
                else
                {
                    query = @"SELECT T0.DocEntry FROM ORDR T0 WHERE T0.CANCELED = 'N' AND T0.U_Cust_PO_No = '" + U_Cust_PO_No + "'";
                }

                MyRs.DoQuery(query);

                while(!(MyRs.EoF))
                {
                    FoundDocEntries.Add(MyRs.Fields.Item(0).Value.ToString());
                    MyRs.MoveNext();
                }

                foreach (XmlNode LineNode in ItemNodes)
                {
                    ValidateOrderLine XmlLine = new ValidateOrderLine();

                    XmlLine.ItemCode = RetriveXMLValue(LineNode, "./ItemCode");
                    XmlLine.U_Store_Id = RetriveXMLValue(LineNode, "./U_x005F_x0020_Store_x005F_x0020_Id");
                    XmlLine.Quantity = Convert.ToDouble(RetriveXMLValue(LineNode, "./Quantity"));

                    ExcelOrder.Lines.Add(XmlLine);
                }

                foreach(string Entry in FoundDocEntries)
                {
                    if (_oConnection._oCompany.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                    {
                        query = @"SELECT T0.""ItemCode"",
                            T0.""SubCatNum"",
                            T0.""Quantity"",
                            T0.""U_Store_Id"",
                            T1.""U_Cust_PO_No"",
                            T1.""CreateDate"",
                            T1.""CreateTS"",
                            T1.""DocNum""
                            FROM RDR1 T0
                            INNER JOIN ORDR T1 ON T0.""DocEntry"" = T1.""DocEntry""
                            WHERE T1.""DocEntry"" = " + Entry;
                    }
                    else
                    {
                        query = @"SELECT T0.ItemCode,
                            T0.SubCatNum,
                            T0.Quantity,
                            T0.U_Store_Id,
                            T1.U_Cust_PO_No,
                            T1.CreateDate,
                            T1.CreateTS,
                            T1.DocNum
                            FROM RDR1 T0
                            INNER JOIN ORDR T1 ON T0.DocEntry = T1.DocEntry
                            WHERE T1.DocEntry = " + Entry;
                    }

                    MyRs.DoQuery(query);

                    if (!(MyRs.EoF))
                    {
                        SapOrder = new ValidateOrder();
                        SapOrder.CreateDate = MyRs.Fields.Item("CreateDate").Value.ToString();
                        SapOrder.CreateTS = MyRs.Fields.Item("CreateTS").Value.ToString();
                        SapOrder.DocNum = MyRs.Fields.Item("DocNum").Value.ToString();

                        while (!(MyRs.EoF))
                        {
                            ValidateOrderLine MyLine = new ValidateOrderLine();
                            MyLine.ItemCode = MyRs.Fields.Item("SubCatNum").Value.ToString();
                            MyLine.U_Store_Id = MyRs.Fields.Item("U_Store_Id").Value.ToString();
                            MyLine.Quantity = MyRs.Fields.Item("Quantity").Value;
                            SapOrder.Lines.Add(MyLine);

                            MyRs.MoveNext();
                        }
                    }

                    DateTime MyDate = new DateTime();
                    MyDate = Convert.ToDateTime(SapOrder.CreateDate);

                    SapOrder.CreateDate = MyDate.ToString("MM-dd-yyyy");

                    foreach (ValidateOrderLine VLine in ExcelOrder.Lines)
                    {
                        ValidateOrderLine TestLine = SapOrder.Lines.Find(x => VLine.ItemCode.Equals(x.ItemCode) && VLine.U_Store_Id.Equals(x.U_Store_Id) && VLine.Quantity == x.Quantity);

                        if (TestLine != null)
                            VLine.Exists = "Y";
                        else
                            VLine.Exists = "N";
                    }

                    if (ExcelOrder.Lines.Exists(x => x.Exists.Equals("N")))
                    {
                        continue;
                    }
                    else
                    {
                        return SapOrder;
                    }
                }

                return null;

            }
            catch (Exception Ex)
            {
                _oCommon.Log("EXCEPTION", "Exception while validating order: " + Ex.Message);
                return null;
            }
           
        }
    }
}
