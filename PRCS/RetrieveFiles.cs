﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PRCS
{
    class RetrieveFiles
    {
        public List<string> GetFileList(ref PRCS.Common _oCommon)
        {
            try
            {
                List<string> filePaths = Directory.GetFiles(Innormax_14.Properties.Settings.Default.DownFolder.ToString(), "*.xlsx").ToList();

                if (filePaths.Count() > 0)
                    return filePaths;
                else
                    return null;
            }
            catch (Exception Ex)
            {
                _oCommon.Log("EXCEPTION", Ex.Message);
                return null;
            }
        }
    }
}
