﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innormax_14;

namespace PRCS
{
    class Connection
    {
        public SAPbobsCOM.Company _oCompany { get; set; }

        public Common _oCommon { get; set; }

        public Connection(ref Common PCommon)
        {
            _oCommon = PCommon;
        }

        public int ConexionSAP()
        {
            int IntentoConexionSAP = 0;

        reconnect:

            IntentoConexionSAP++;

            if (_oCompany == null)
                _oCompany = new SAPbobsCOM.Company();

            if (_oCompany.Connected)
                _oCompany.Disconnect();

            _oCompany.Server = Innormax_14.Properties.Settings.Default.Server;
            _oCompany.DbPassword = Innormax_14.Properties.Settings.Default.DBPassword;
            _oCompany.DbUserName = Innormax_14.Properties.Settings.Default.DBUsername;
            _oCompany.UserName = Innormax_14.Properties.Settings.Default.UserName;
            _oCompany.Password = Innormax_14.Properties.Settings.Default.Password;
            _oCompany.CompanyDB = Innormax_14.Properties.Settings.Default.CompanyDB;
            _oCompany.LicenseServer = Innormax_14.Properties.Settings.Default.LicenseServer;

            switch (Innormax_14.Properties.Settings.Default.DbType)
            {
                case "2005":
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                    break;
                case "2008":
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                    break;
                case "2012":
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "2014":
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "2016":
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                    break;
                case "2017":
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                    break;
                case "2019":
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2019;
                    break;
                case "HANA":
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                    break;
                default:
                    _oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                    break;
            }

            int ret = _oCompany.Connect();

            if (ret != 0)
            {
                _oCommon.Log("ERROR", "Failed trying to connect: " + _oCompany.GetLastErrorDescription() + " - " + _oCompany.GetLastErrorCode() + ". Attempt number: " + IntentoConexionSAP.ToString());

                if (IntentoConexionSAP < Convert.ToInt16(Innormax_14.Properties.Settings.Default.ConnectionAttemps))
                    goto reconnect;

                return ret;
            }
            else
            {
                _oCommon.Log("SUCCESS", "Connected to : " + _oCompany.Server + "@" + _oCompany.CompanyDB + " - User: " + _oCompany.UserName + " - Attempt number: " + IntentoConexionSAP.ToString());
                IntentoConexionSAP = 0;
                return ret;
            }
        }
    }
}
