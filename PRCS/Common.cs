﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace PRCS
{
    public class Common
    {
        //Variable que almacena la ruta desde donde se ejecuta el .exe
        private static string sRoute = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

        //Método: creación y registro de Logs
        public void Log(string sEstatus, string sMsg, [CallerMemberName] string sMethod = "", [CallerLineNumber] int iLine = 0)
        {
            try
            {
                //Crea la carpeta de Logs
                Directory.CreateDirectory(sRoute + "\\Logs\\");

                if (sEstatus.Equals("Header"))
                {
                    //Crea el archivo .txt y agrega los mensajes
                    using (StreamWriter w = File.AppendText(sRoute + "\\Logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                    {
                        w.WriteLine("{0} {1} - {2}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), sMsg);
                    }
                }
                else if (sEstatus.Equals("Record"))
                {
                    //Crea el archivo .txt y agrega los mensajes
                    using (StreamWriter w = File.AppendText(sRoute + "\\Logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                    {
                        w.WriteLine("{0} {1} - {2}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), sMsg);
                        w.WriteLine("****************************************************************************************");
                    }
                }
                else if (sEstatus.Equals("Start"))
                {
                    //Crea el archivo .txt y agrega los mensajes
                    using (StreamWriter w = File.AppendText(sRoute + "\\Logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                    {
                        w.WriteLine("**************************************************************************************************************************************");
                        w.WriteLine("{0} {1} - {2}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), sMsg);     
                    }
                }
                else if (sEstatus.Equals("End"))
                {
                    //Crea el archivo .txt y agrega los mensajes
                    using (StreamWriter w = File.AppendText(sRoute + "\\Logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                    {
                        w.WriteLine("{0} {1} - {2}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), sMsg);
                        w.WriteLine("**************************************************************************************************************************************");
                    }
                }
                else
                {
                    //Crea el archivo .txt y agrega los mensajes
                    using (StreamWriter w = File.AppendText(sRoute + "\\Logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                    {
                        w.Write("{0} Log Entry: {1} {2} , {3}({4}) - ", sEstatus, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), sMethod, iLine);
                        w.WriteLine(sMsg);
                    }
                }

                

            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR - " + e.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
