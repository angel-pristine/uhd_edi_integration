﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OBJT
{
    public class GroupRow
    {
        public string ParentKey { get; set; }
        public string LineNum { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public string Quantity { get; set; }
        public string ShipDate { get; set; }
        public string Price { get; set; }
        public string Currency { get; set; }
        public string WarehouseCode { get; set; }
        public string TaxCode { get; set; }
        public string LineType { get; set; }
        public string U_x0020_Store_x0020_Id { get; set; }
        public string U_x0020_Customer_x0020_PO { get; set; }
    }
}
