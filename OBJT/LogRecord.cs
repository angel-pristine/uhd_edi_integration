﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OBJT
{
    class LogRecord
    {
        public LogRecord()
        {

        }
        public string PurchaseOrderNumber { get; set; }
        public string SettlementId { get; set; }
        public string Status { get; set; }
        public string LogDate = DateTime.Now.ToShortDateString();
        public string CardCode { get; set; }
        public string LogTime = DateTime.Now.ToShortTimeString();
        public string FileName { get; set; }
        public string DocumentDate { get; set; }
        public string DocEntry { get; set; }
        public string DocNum { get; set; }
        public string Owner { get; set; }
        public string Description { get; set; }



    }
}
