﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OBJT
{
    class ValidateOrder
    {
        public List<ValidateOrderLine> Lines = new List<ValidateOrderLine>();
        public string U_Cust_PO_No { get; set; }
        public string CreateDate { get; set; }
        public string CreateTS { get; set; }
        public string DocNum { get; set; }
    }

    class ValidateOrderLine
    { 
        public string ItemCode { get; set; }
        public double Quantity { get; set; }
        public string U_Store_Id { get; set; }
        public string Exists { get; set; }
    }
}
