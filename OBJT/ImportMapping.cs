﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OBJT
{
    class ImportMapping
    {
        public int Code { get; set; }
        public string Name { get; set; }
        public string IntegrationName { get; set; }
        public string Object { get; set; }
        public string Level { get; set; }
        public string XML_Source { get; set; }
        public string SAP_Target { get; set; }
        public string LookUp { get; set; }
        public string Parameters { get; set; }
        public string Validate { get; set; }
        public string SkipIfEmpty { get; set; }
        public string Enable { get; set; }
        public string MaxOccurs { get; set; }


    }
}
