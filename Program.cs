﻿using System;
using PRCS;
using System.IO;
using System.Collections.Generic;

namespace Innormax_14
{
    class Program
    {
        private static Common _oCommon;
        private static Connection _oConnection;
        private static string SettlementId = string.Empty;

        static void Main(string[] args)
        {
            _oCommon = new Common();
            _oCommon.Log("INFO", "Process Started");
            RetrieveFiles RTFiles = new RetrieveFiles();
            List<string> files = RTFiles.GetFileList(ref _oCommon);

            if (files != null)
            {
                _oCommon.Log("INFO", files.Count.ToString() + " Files found on " + Properties.Settings.Default.DownFolder);

                // Attemp to Connect to SAP
                _oConnection = new Connection(ref _oCommon);
                if (_oConnection.ConexionSAP() == 0)
                {
                    SettlementId = Convert.ToString(long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff")));
                    _oCommon.Log("INFO", "Execution Settlement: " + SettlementId);

                    ProcessFiles _oProcessFiles = new ProcessFiles(files, ref _oCommon, ref _oConnection, SettlementId);

                    // Reading import mapping table
                    _oProcessFiles.ReadMapTable();
                    _oProcessFiles.FormatDocumentXLSX();
                }
            }
            else
            {
                _oCommon.Log("INFO", "No files found on: " + Properties.Settings.Default.DownFolder);
            }

            _oCommon.Log("INFO", "Process End");
        }
    }
}
