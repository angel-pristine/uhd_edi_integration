﻿using System;
using System.Xml;
using PRCS;
using OBJT;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BASE
{
    class Utilities
    {
        SAPbobsCOM.Recordset MyRs;
        SAPbobsCOM.Company UCompany;
        Common _oCommon;
        XmlNode ValueNode;

        public Utilities(SAPbobsCOM.Company PCompany, Common PCommon)
        {
            MyRs = PCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            _oCommon = PCommon;
            UCompany = PCompany;
        }

        protected string GetFieldValue(string PQuery)
        {
            try
            {
                if (MyRs != null)
                {
                    MyRs.DoQuery(PQuery);

                    if (!(MyRs.EoF))
                        return MyRs.Fields.Item(0).Value.ToString();
                }

                return null;

            }
            catch (Exception Ex)
            {
                _oCommon.Log("EXCEPTION", Ex.Message);
                return null;
            }


            
        }

        protected string RetriveXMLValue(XmlNode SourceXML, string PXpath)
        {
            try
            {
                ValueNode = SourceXML.SelectSingleNode(PXpath);
                if (ValueNode == null)
                    return null;
                else
                    return ValueNode.InnerText.ToString();
            }
            catch (Exception Ex)
            {
                _oCommon.Log("EXCEPTION", Ex.Message);
                return null;
            }
            
        }

        protected string GetSingleValueObject(Type sourceObjectType, XmlNode sourceObject, string source, string target, string lookup, string parameters, string validate, string sapObjectType)
        {
            string value = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(lookup))
                {
                    string[] parameterFields = parameters.Split(';');
                    string[] parameterValues = new string[parameterFields.Length];
                    for (int i = 0; i < parameterFields.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(parameterFields[i]))
                        {
                            XmlNode obj = sourceObject.SelectSingleNode(parameterFields[i]);
                            if (obj == null)
                                throw new Exception(string.Format("Field {0} is not in the source data. Available fields are: {1}", source, sourceObject.ChildNodes.ToString()));
                            parameterValues[i] = obj.InnerText.ToString().Replace("'", "''");
                        }
                    }

                    string query = string.Format(lookup, parameterValues);
                    value = GetFieldValue(query);

                    if (!string.IsNullOrEmpty(validate))
                    {
                        if (validate.Equals("Y"))
                        {
                            if (string.IsNullOrEmpty(value))
                                throw new Exception(string.Format("Lookup failed. Source:{0}, Target:{1}, Query:{2}", source, target, query));
                        }
                    }
                }
                else
                {
                    XmlNode obj = sourceObject.SelectSingleNode(source);
                    if (obj == null)
                        throw new Exception(string.Format("Field {0} is not in the source data. Available fields are: {1}", source, sourceObject.ChildNodes.ToString()));
                    value = obj.InnerText.ToString();
                }
            }
            catch (Exception ex)
            {
                _oCommon.Log("ERROR", string.Format("Error getting header value object. Source:{0}, Target:{1}, Error:{2}", source, target, ex.Message));
            }

            return value;
        }

        protected string GetLineValueObject(XmlNode parentObject, XmlNode sourceObject, string source, string target, string lookup, string parameters, string validate, string sapObjectType)
        {
            string value = string.Empty;
            XmlNode obj = null;
            try
            {
                if (!string.IsNullOrEmpty(lookup))
                {
                    string[] parameterFields = parameters.Split(';');
                    string[] parameterValues = new string[parameterFields.Length];
                    for (int i = 0; i < parameterFields.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(parameterFields[i]))
                        {
                            if (parameterFields[i].StartsWith("#"))
                            {
                                parameterFields[i] = parameterFields[i].Trim('#');
                                obj = sourceObject.SelectSingleNode(parameterFields[i]);
                                if (obj == null)
                                    throw new Exception(string.Format("Field {0} is not in the source data. Available fields are: {1}", source, sourceObject.ChildNodes.ToString()));
                                parameterValues[i] = obj.InnerText.ToString().Replace("'", "''");
                            }
                            else
                            {
                                obj = parentObject.SelectSingleNode(parameterFields[i]);
                                if (obj == null)
                                    throw new Exception(string.Format("Field {0} is not in the source data. Available fields are: {1}", source, sourceObject.ChildNodes.ToString()));
                                parameterValues[i] = obj.InnerText.ToString().Replace("'", "''");
                            }

                        }
                    }

                    string query = string.Format(lookup, parameterValues);
                    value = GetFieldValue(query);

                    if (!string.IsNullOrEmpty(validate))
                    {
                        if (validate.Equals("Y"))
                        {
                            if (string.IsNullOrEmpty(value))
                                throw new Exception(string.Format("Lookup failed. Source:{0}, Target:{1}, Query:{2}", source, target, query));
                        }
                    }
                }
                else
                {
                    if (source.StartsWith("#"))
                    {
                        source = source.Trim('#');
                        obj = sourceObject.SelectSingleNode(source);
                        if (obj == null)
                            throw new Exception(string.Format("Field {0} is not in the source data. Available fields are: {1}", source, sourceObject.ChildNodes.ToString()));
                        value = obj.InnerText.ToString().Replace("'", "''");
                    }
                    else
                    {
                        obj = parentObject.SelectSingleNode(source);
                        if (obj == null)
                            throw new Exception(string.Format("Field {0} is not in the source data. Available fields are: {1}", source, sourceObject.ChildNodes.ToString()));
                        value = obj.InnerText.ToString().Replace("'", "''");
                    }
                }
            }
            catch (Exception ex)
            {
                _oCommon.Log("ERROR", string.Format("Error getting Line value object. Source:{0}, Target:{1}, Error:{2}", source, target, ex.Message));
            }
            finally
            {
                GC.Collect();
            }

            return value;
        }

        protected void LogTable(LogRecord PRecord)
        {
            try
            {
                SAPbobsCOM.UserTable LogTable = UCompany.UserTables.Item("INN_EDI_FIL_LOG");
                string Code = string.Empty;

                if (UCompany.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                    Code = GetFieldValue(string.Format(@"SELECT ""Code"" FROM ""@INN_EDI_FIL_LOG"" WHERE ""Name"" = '{0}'", PRecord.PurchaseOrderNumber));
                else
                    Code = GetFieldValue(string.Format(@"SELECT Code FROM [@INN_EDI_FIL_LOG] WHERE Name = '{0}'", PRecord.PurchaseOrderNumber));

                if (LogTable.GetByKey(Code))
                {
                    if (PRecord.SettlementId != null)
                        LogTable.UserFields.Fields.Item("U_SettlementId").Value = PRecord.SettlementId;
                    if (PRecord.Status != null)
                        LogTable.UserFields.Fields.Item("U_Status").Value = PRecord.Status;
                    if (PRecord.LogDate != null)
                        LogTable.UserFields.Fields.Item("U_LogDate").Value = PRecord.LogDate;
                    if (PRecord.CardCode != null)
                        LogTable.UserFields.Fields.Item("U_CardCode").Value = PRecord.CardCode;
                    if (PRecord.LogTime != null)
                        LogTable.UserFields.Fields.Item("U_LogTime").Value = PRecord.LogTime;
                    if (PRecord.FileName != null)
                        LogTable.UserFields.Fields.Item("U_FileName").Value = PRecord.FileName;
                    if (PRecord.DocumentDate != null)
                        LogTable.UserFields.Fields.Item("U_DocumentDate").Value = PRecord.DocumentDate;
                    if (PRecord.DocEntry != null)
                        LogTable.UserFields.Fields.Item("U_DocEntry").Value = PRecord.DocEntry;
                    if (PRecord.DocNum != null)
                        LogTable.UserFields.Fields.Item("U_DocNum").Value = PRecord.DocNum;
                    if (PRecord.Owner != null)
                        LogTable.UserFields.Fields.Item("U_Owner").Value = PRecord.Owner;
                    if (PRecord.Description != null)
                    {
                        LogTable.UserFields.Fields.Item("U_Description").Value = PRecord.Description;
                    }
                    else if (PRecord.Status == "S")
                    {
                        LogTable.UserFields.Fields.Item("U_Description").Value = "";
                    }
                        

                    if (LogTable.Update() == 0)
                    {
                        _oCommon.Log("INFO", "LOG Record succesfully updated on UDT");
                    }
                    else
                    {
                        _oCommon.Log("ERROR", UCompany.GetLastErrorCode() + " - " + UCompany.GetLastErrorDescription());
                    }

                }
                else
                {
                    if (PRecord.PurchaseOrderNumber != null)
                        LogTable.Name = PRecord.PurchaseOrderNumber;
                    if (PRecord.SettlementId != null)
                        LogTable.UserFields.Fields.Item("U_SettlementId").Value = PRecord.SettlementId;
                    if (PRecord.Status != null)
                        LogTable.UserFields.Fields.Item("U_Status").Value = PRecord.Status;
                    if (PRecord.LogDate != null)
                        LogTable.UserFields.Fields.Item("U_LogDate").Value = PRecord.LogDate;
                    if (PRecord.CardCode != null)
                        LogTable.UserFields.Fields.Item("U_CardCode").Value = PRecord.CardCode;
                    if (PRecord.LogTime != null)
                        LogTable.UserFields.Fields.Item("U_LogTime").Value = PRecord.LogTime;
                    if (PRecord.FileName != null)
                        LogTable.UserFields.Fields.Item("U_FileName").Value = PRecord.FileName;
                    if (PRecord.DocumentDate != null)
                        LogTable.UserFields.Fields.Item("U_DocumentDate").Value = PRecord.DocumentDate;
                    if (PRecord.DocEntry != null)
                        LogTable.UserFields.Fields.Item("U_DocEntry").Value = PRecord.DocEntry;
                    if (PRecord.DocNum != null)
                        LogTable.UserFields.Fields.Item("U_DocNum").Value = PRecord.DocNum;
                    if (PRecord.Owner != null)
                        LogTable.UserFields.Fields.Item("U_Owner").Value = PRecord.Owner;
                    if (PRecord.Description != null)
                        LogTable.UserFields.Fields.Item("U_Description").Value = PRecord.Description;

                    if (LogTable.Add() == 0)
                    {
                        _oCommon.Log("INFO", "Record succesfully logged on UDT");
                    }
                    else
                    {
                        _oCommon.Log("ERROR", UCompany.GetLastErrorCode() + " - " + UCompany.GetLastErrorDescription());
                    }
                }


            }
            catch (Exception Ex)
            {
                _oCommon.Log("EXCEPTION", Ex.ToString());
            }
        }
    }
}
